# AuctionOnline

# FINAL-PROJECT

Gitlab lưu trữ hoạt động trong đồ án cuối kỳ môn phát triển ứng dụng web

========= Note Folder=========
- source: Nơi chứa source code chạy bằng nodejs để thực hiện các lệnh api với ajax.
- Documents: Nơi chứa các files documents, báo cáo, phân tích,....
- template: file template html để đưa vào project nodejs

========== Hướng dẫn cách chạy ===========
- cài đặt app: npm express --view=hbs
- npm install
- npm install express-handlebars
- npm start
- Chạy với port 8080: localhost:8080

========================================

- Chương trình thực hiện  đồ án cuối kỳ - K17 năm học 2019 
- Thực hiện bởi nhóm gồm các thành viên:
-  + 1712341 - Phan Thị Mỹ Diêm
-  + 1712... - Nguyễn Hoàng Thiên Ân
-  + 1712... - Nguyễn Đăng Hưng
-  + 1512... - Đoàn Minh Toàn
- Ngày bắt đầu 12/08/2019 -> ../../2019

========================================
------- Các bước thực hiện -------------

1. Tạo Working folder trên google drive để lưu trữ file dạng online (Lưu template báo cáo, viết báo cáo, file doc phân tích yêu cầu,...)
2. Tạo note phân tích trong google keep để note lại các phần cần làm. (Note lại các bước cần làm, các phần cần thực hiện trước khi bắt tay vào code.)
3. Các công cụ sử dụng:
   - Công cụ Google Doc -> Dùng để viết báo cáo
   - Công cụ GitKraken -> sử dụng pull, push code và quản lý code trên Gitlab - Gitlab thuộc dạng private - Cần được add và accept mới có thể xem được.
   - Công cụ Visual code -> Viết code và thực hiện chạy thử, debug.
   - Browser Firefox, chrome.
4. Các công nghệ áp dụng:
   - Node js - express
   - Ajax
   - Bootstrap, html5, css,...
5. Các trang hỗ trợ.
   - stackoverflow
   - w3school
   - getbootstrap
   - facebook