var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var exphbs = require('express-handlebars');
var handlebars = require('handlebars')

var homeRouter = require('./routes/home');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs',
})


handlebars.registerHelper('times', function(n, block) {
    var accum = '';
    for (var i = 1; i <= n; ++i)
        accum += block.fn(i);
    return accum;
});

handlebars.registerHelper({
    eq: function(v1, v2) {
        return v1 === v2;
    },
    ne: function(v1, v2) {
        return v1 !== v2;
    },
    lt: function(v1, v2) {
        return v1 < v2;
    },
    gt: function(v1, v2) {
        return v1 > v2;
    },
    lte: function(v1, v2) {
        return v1 <= v2;
    },
    gte: function(v1, v2) {
        return v1 >= v2;
    },
    and: function() {
        return Array.prototype.slice.call(arguments).every(Boolean);
    },
    or: function() {
        return Array.prototype.slice.call(arguments, 0, -1).some(Boolean);
    }
});

app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');

app.set('views', path.join(__dirname, 'views'));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', homeRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;