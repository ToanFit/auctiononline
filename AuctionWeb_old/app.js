const express = require('express'),
app = express(),
exhbs = require('express-handlebars');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//
const port = 3000;
app.listen(port, () => { console.log(`App listening on port ${port}!`); });
app.use(express.static(__dirname + '/public'));
//
const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs'
});
app.engine('hbs', hbs.engine );
app.set('view engine', 'hbs');
//
app.use('/', (req, res) => {
    res.render('home')  
});